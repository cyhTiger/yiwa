# coding: utf8

""""""
import os
from apps import app
from flask import render_template
from yiwa.db import Sqlite3DB
from yiwa.settings import BASE_DIR


@app.route("/self_discipline/week")
def week():
    s3db = Sqlite3DB(os.path.join(BASE_DIR,
                                  "apps",
                                  "self_discipline",
                                  "self_discipline.s3db"))
    return render_template("self_discipline/week.html")
