#Python36
# Run pip install --requirement=requirements.txt to install al requirements

Flask        ==1.0.3
Flask-SocketIO      ==4.1.0
baidu-aip    ==2.2.13.0
selenium     ==3.141.0
jieba        ==0.39
PyYAML       ==5.1.1
ruamel.yaml  ==0.15.97
Pillow       ==6.1.0
qrcode       ==6.1
pyttsx3      ==2.71
PyAudio      ==0.2.11
SpeechRecognition   ==3.8.1
pocketsphinx        ==0.1.15
