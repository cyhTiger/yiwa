# coding: utf8

""""""
import platform
import signal
from importlib import import_module
from yiwa.db import DataConveyor
from asr.configs import MODEL_FILE

try:
    systems = {"Darwin": "osx"}
    snowboydecoder = import_module(
        f"snowboy.{systems.get(platform.system())}.snowboydecoder")
except Exception as e:
    print(e)

interrupted = False


def signal_handler(signal, frame):
    global interrupted
    interrupted = True


def interrupt_callback():
    global interrupted
    return interrupted


def wakeup():
    global interrupted
    interrupted = True
    snowboydecoder.play_audio_file()


def wakeup_waiting():
    """唤醒中"""

    # capture SIGINT signal, e.g., Ctrl+C
    signal.signal(signal.SIGINT, signal_handler)
    detector = snowboydecoder.HotwordDetector(MODEL_FILE, sensitivity=0.5)

    data_conveyor = DataConveyor()
    print("Start Recording...")
    data_conveyor.listening()

    # main loop
    detector.start(detected_callback=wakeup,
                   interrupt_check=interrupt_callback,
                   sleep_time=0.03)

    detector.terminate()
    print("Recording Done...")
    data_conveyor.listened()

    return True


if __name__ == "__main__":
    print(wakeup_waiting())
